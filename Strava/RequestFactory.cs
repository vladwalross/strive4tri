﻿using RestSharp.Portable;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable.OAuth2.Infrastructure;

namespace Strive4Tri.Strava
{
    public class RequestFactory : IRequestFactory
    {
        #region IRequestFactory implementation

        public IRestClient CreateClient()
        {
            return new RestClient();
        }

        public IRestRequest CreateRequest(string resource)
        {
            return new RestRequest(resource);
        }

        #endregion
    }
}
