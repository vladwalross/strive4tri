﻿using System;
using System.Net;
using System.Threading.Tasks;
using RestSharp.Portable;
using RestSharp.Portable.OAuth2;
using RestSharp.Portable.OAuth2.Infrastructure;
using System.Diagnostics;
using Microsoft.AspNetCore.Http;
using Strive4Tri.Strive;
using StravaSharp;

namespace Strive4Tri.Strava
{
    public class StravaAuthenticator : OAuth2Authenticator
    {
        /// <summary>
        /// The access token that was received from the server.
        /// </summary>
        public string AccessToken
        {
            get
            {
                var accessToken = _httpContext.Session.GetString("AccessToken");
                return accessToken == null ? null : accessToken as string;
            }
            set
            {
                _httpContext.Session.SetString("AccessToken", value);
            }
        }

        public bool IsAuthenticated => AccessToken != null;
        private readonly HttpContext _httpContext;

        public StravaAuthenticator(OAuth2Client client) : base(client)
        {
        }

        public StravaAuthenticator(OAuth2Client client, HttpContext httpContext) : base(client)
        {
            _httpContext = httpContext;
        }

        public async Task<Uri> GetLoginLinkUri()
        {
            var uri = await Client.GetLoginLinkUri();
            return new Uri(uri);
        }

        public async Task<bool> OnPageLoaded(string absoluteUri, string query)
        {
            if (absoluteUri.StartsWith(Client.Configuration.RedirectUri))
            {
                Debug.WriteLine("Navigated to redirect url.");
                var parameters = query.Remove(0, 1).ParseQueryString(); // query portion of the response
                await Client.GetUserInfo(parameters);

                if (!string.IsNullOrEmpty(Client.AccessToken))
                {
                    AccessToken = Client.AccessToken;
                    return true;
                }
            }

            return false;
        }

        public override bool CanPreAuthenticate(IRestClient client, IRestRequest request, ICredentials credentials)
        {
            return true;
        }

        public override bool CanPreAuthenticate(IHttpClient client, IHttpRequestMessage request, ICredentials credentials)
        {
            return false;
        }

        public override async Task PreAuthenticate(IRestClient client, IRestRequest request, ICredentials credentials)
        {
            if (!string.IsNullOrEmpty(AccessToken))
                request.AddHeader("Authorization", "Bearer " + AccessToken);
        }

        public override Task PreAuthenticate(IHttpClient client, IHttpRequestMessage request, ICredentials credentials)
        {
            throw new NotImplementedException();
        }

        public static StravaAuthenticator CreateAuthenticator(string redirectUrl, HttpContext httpContext)
        {
            var clientId = Settings.Get("Strava:Credentials:ClientId");
            var clientSecret = Settings.Get("Strava:Credentials:ClientSecret");
            var config = new RestSharp.Portable.OAuth2.Configuration.RuntimeClientConfiguration
            {
                IsEnabled = false,
                ClientId = clientId,
                ClientSecret = clientSecret,
                RedirectUri = redirectUrl,
                Scope = "view_private",
            };
            var client = new StravaClient(new RequestFactory(), config);

            return new StravaAuthenticator(client, httpContext);
        }
    }
}
