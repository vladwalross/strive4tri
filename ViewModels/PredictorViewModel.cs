﻿using System.Collections.Generic;

namespace Strive4Tri.ViewModels
{
    public class PredictorViewModel
    {
        public PredictorViewModel()
        {
            RunningPerformance = new List<ActivityBase>();
            CyclingPerformance = new List<ActivityBase>();
            SwimmingPerformance = new List<ActivityBase>();
        }

        public List<ActivityBase> RunningPerformance;
        public List<ActivityBase> CyclingPerformance;
        public List<ActivityBase> SwimmingPerformance;
    }
}
