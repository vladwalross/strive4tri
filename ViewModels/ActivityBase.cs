﻿using System;

namespace Strive4Tri.ViewModels
{
    public class ActivityBase
    {
        public int Distance { get; set; }
        public TimeSpan Time { get; set; }
        public double Performance { get; set; }
    }
}
