﻿using Newtonsoft.Json;
using StravaSharp;
using Strive4Tri.Strava;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Strive4Tri.Strive
{
    public class Predictor
    {
        private readonly StravaAuthenticator _authenticator;
        private IEnumerable<ActivitySummary> _runs, _rides, _swims;
        private bool _loadSample = false;

        public IEnumerable<ActivitySummary> Runs
        {
            get
            {
                if (_runs == null)
                {
                    if (_loadSample)
                    {
                        LoadSampleData();
                    }
                    else
                    {
                        var task = LoadData();
                        task.Wait();
                    }
                }

                return _runs;
            }
        }

        public IEnumerable<ActivitySummary> Rides
        {
            get
            {
                if (_rides == null)
                {
                    if (_loadSample)
                    {
                        LoadSampleData();
                    }
                    else
                    {
                        var task = LoadData();
                        task.Wait();
                    }
                }

                return _rides;
            }
        }

        public IEnumerable<ActivitySummary> Swims
        {
            get
            {
                if (_swims == null)
                {
                    if (_loadSample)
                    {
                        LoadSampleData();
                    }
                    else
                    {
                        var task = LoadData();
                        task.Wait();
                    }
                }

                return _swims;
            }
        }

        public Predictor(StravaAuthenticator authenticator)
        {
            _authenticator = authenticator;
            _loadSample = false;
        }

        public Predictor() => _loadSample = true;

        private async Task LoadData()
        {
            var currentPage = 0;
            var activities = new List<ActivitySummary>();
            var oldest = DateTime.Now;
            var days = Convert.ToInt32(Settings.Get("Strava:PastDays"));
            var startDate = DateTime.Now.AddDays(-1 * days); // Only get activities from last N days
            var client = new Client(_authenticator);

            try
            {
                do
                {
                    var response = await client.Activities.GetAthleteActivities(currentPage++, 100);

                    if (response.Count == 0)
                    {
                        break;
                    }

                    oldest = response.Min(m => m.StartDate);
                    activities.AddRange(response);
                } while (oldest > startDate);
            }
            catch (Exception)
            {
                activities = new List<ActivitySummary>();
            }

            activities = (from activity in activities where activity.StartDate >= startDate select activity).ToList();
            _runs = from run in activities where run.Type == ActivityType.Run select run;
            _rides = from ride in activities where ride.Type == ActivityType.Ride || ride.Type == ActivityType.VirtualRide select ride;
            _swims = from swim in activities where swim.Type == ActivityType.Swim select swim;
        }

        private void LoadSampleData()
        {
            var activities = new List<ActivitySummary>();

            try
            {
                using (var reader = new StreamReader(Directory.GetCurrentDirectory() + @"\sample.json"))
                {
                    var json = reader.ReadToEnd();
                    activities = JsonConvert.DeserializeObject<List<ActivitySummary>>(json);
                }
            }
            catch (Exception)
            {
                activities = new List<ActivitySummary>();
            }

            _runs = from run in activities where run.Type == ActivityType.Run select run;
            _rides = from ride in activities where ride.Type == ActivityType.Ride || ride.Type == ActivityType.VirtualRide select ride;
            _swims = from swim in activities where swim.Type == ActivityType.Swim select swim;
        }

        // Calculates pace for most typical running distances
        public List<KeyValuePair<int, double>> GetRunningPerformance()
        {
            var dictionary = new ConcurrentDictionary<int, double>();
            dictionary.TryAdd(0, 1);
            dictionary.TryAdd(5, 5000);
            dictionary.TryAdd(10, 10000);
            dictionary.TryAdd(20, 20000);
            dictionary.TryAdd(21, 21000);
            dictionary.TryAdd(42, 42000);

            foreach (var pair in dictionary)
            {
                // Get sum of distances and times for activities longer than distance specified
                double sumDistances = (from run in Runs where run.Distance >= pair.Value select run).Sum(s => s.Distance);
                double sumTimes = (from run in Runs where run.Distance >= pair.Value select run).Sum(s => s.ElapsedTime);

                // Check division by 0
                if (sumTimes == 0 || sumDistances == 0)
                {
                    dictionary[pair.Key] = 0;
                }
                else
                {
                    // Running performance is meassured in minutes per kilometer (aka pace), so we go from meters per second to kilometers per hour and then to pace.
                    double pace = sumDistances / sumTimes;
                    pace *= 3.6;
                    pace = 60 / pace;
                    dictionary[pair.Key] = pace;
                }
            }

            var performance = dictionary.ToList();
            performance.Sort((pair1, pair2) => pair1.Key.CompareTo(pair2.Key));
            return performance;
        }

        // Calculates speed for most typical cycling distances
        public List<KeyValuePair<int, double>> GetCyclingPerformance()
        {
            var dictionary = new ConcurrentDictionary<int, double>();
            dictionary.TryAdd(0, 1);
            dictionary.TryAdd(20, 20000);
            dictionary.TryAdd(40, 40000);
            dictionary.TryAdd(80, 80000);
            dictionary.TryAdd(90, 90000);
            dictionary.TryAdd(180, 180000);

            foreach (var pair in dictionary)
            {
                // Get sum of distances and times for activities longer than distance specified
                double sumDistances = (from ride in Rides where ride.Distance >= pair.Value select ride).Sum(s => s.Distance);
                double sumTimes = (from ride in Rides where ride.Distance >= pair.Value select ride).Sum(s => s.ElapsedTime);

                // Check division by 0
                if (sumTimes == 0 || sumDistances == 0)
                {
                    dictionary[pair.Key] = 0;
                }
                else
                {
                    // Cycling performance is meassured in kilometers per hour (aka speed), so we go from meters per second to kilometers per hour.
                    double speed = sumDistances / sumTimes;
                    speed *= 3.6;
                    dictionary[pair.Key] = speed;
                }
            }

            var performance = dictionary.ToList();
            performance.Sort((pair1, pair2) => pair1.Key.CompareTo(pair2.Key));
            return performance;
        }

        // Calculates pace for most typical swimming distances
        public List<KeyValuePair<int, double>> GetSwimmingPerformance()
        {
            var dictionary = new ConcurrentDictionary<int, double>();
            dictionary.TryAdd(0, 1);
            dictionary.TryAdd(750, 750);
            dictionary.TryAdd(1500, 1500);
            dictionary.TryAdd(3000, 3000);
            dictionary.TryAdd(1900, 1900);
            dictionary.TryAdd(3800, 3800);

            foreach (var pair in dictionary)
            {
                // Get sum of distances and times for activities longer than distance specified
                double sumDistances = (from swim in Swims where swim.Distance >= pair.Value select swim).Sum(s => s.Distance);
                double sumTimes = (from swim in Swims where swim.Distance >= pair.Value select swim).Sum(s => s.ElapsedTime);

                // Check division by 0
                if (sumTimes == 0 || sumDistances == 0)
                {
                    dictionary[pair.Key] = 0;
                }
                else
                {
                    // Swimming performance is meassured in minutes per each 100 meters (aka pace), so we go from meters per second to seconds per 100 meters and then to pace.
                    double pace = sumDistances / sumTimes;
                    pace = 100 / pace;
                    pace /= 60;
                    dictionary[pair.Key] = pace;
                }
            }

            var performance = dictionary.ToList();
            performance.Sort((pair1, pair2) => pair1.Key.CompareTo(pair2.Key));
            return performance;
        }
    }
}
