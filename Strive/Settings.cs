﻿using Microsoft.Extensions.Configuration;
using System.IO;

namespace Strive4Tri.Strive
{
    public static class Settings
    {
        private static IConfiguration Configuration { get; set; }
 
        public static string Get(string key)
        {
            if (Configuration == null)
            {
                Configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
            }

            return Configuration[key];
        }
    }
}
