﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Strive4Tri.Models;

namespace Strive4Tri.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "What Strive4Tri is all about";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Feel free to contact me. Here is how you can get to me";

            return View();
        }

        public IActionResult Help()
        {
            ViewData["Message"] = "All the questions of the history of mankind answered, for true";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
