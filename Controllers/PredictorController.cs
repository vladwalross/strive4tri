﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Strive4Tri.ViewModels;
using Strive4Tri.Strava;
using System.Linq;
using System.Collections.Generic;
using Strive4Tri.Strive;
using System;

namespace Strive4Tri.Controllers
{
    public class PredictorController : Controller
    {
        private string RedirectUrl
        {
            get
            {
                return $"{Request.Scheme}://{Request.Host}/Predictor/Callback";
            }
        }

        // GET: Predictors
        public async Task<ActionResult> Index()
        {
            var authenticator = StravaAuthenticator.CreateAuthenticator(RedirectUrl, HttpContext);

            if (!authenticator.IsAuthenticated)
            {
                var loginUri = await authenticator.GetLoginLinkUri();
                return Redirect(loginUri.AbsoluteUri);
            }

            var predictor = new Predictor(authenticator);
            return ProcessActionResult(predictor);
        }

        public ActionResult Sample()
        {
            var predictor = new Predictor();
            return ProcessActionResult(predictor);
        }

        private ActionResult ProcessActionResult(Predictor predictor)
        {
            var viewModel = new PredictorViewModel();
            var runningPerformance = predictor.GetRunningPerformance();
            var cyclingPerformance = predictor.GetCyclingPerformance();
            var swimmingPerformance = predictor.GetSwimmingPerformance();
            
            if (runningPerformance.Sum(s => s.Value) == 0)
            {
                viewModel.RunningPerformance = null;
            }
            else
            {
                for (var i = 0; i < runningPerformance.Count; i++)
                {
                    if (runningPerformance[i].Value > 0)
                    {
                        viewModel.RunningPerformance.Add(new ActivityBase
                        {
                            Distance = runningPerformance[i].Key,
                            Performance = runningPerformance[i].Value,
                            Time = TimeSpan.FromMinutes(runningPerformance[i].Key * runningPerformance[i].Value)
                        });
                    }

                    if (i < runningPerformance.Count - 1 && runningPerformance[i + 1].Value == 0)
                    {
                        // If there's no information for a given distance, we just use the previous distance (but 5% slower, just an arbitrary value, no science here)
                        viewModel.RunningPerformance.Add(new ActivityBase
                        {
                            Distance = runningPerformance[i + 1].Key,
                            Performance = viewModel.RunningPerformance[i].Performance * 1.05,
                            Time = TimeSpan.FromMinutes(runningPerformance[i + 1].Key * viewModel.RunningPerformance[i].Performance * 1.05)
                        });
                    }
                }

                viewModel.RunningPerformance.RemoveAt(0);
            }

            if (cyclingPerformance.Sum(s => s.Value) == 0)
            {
                viewModel.CyclingPerformance = null;
            }
            else
            {
                for (var i = 0; i < cyclingPerformance.Count; i++)
                {
                    if (cyclingPerformance[i].Value > 0)
                    {
                        viewModel.CyclingPerformance.Add(new ActivityBase
                        {
                            Distance = cyclingPerformance[i].Key,
                            Performance = cyclingPerformance[i].Value,
                            Time = TimeSpan.FromHours(cyclingPerformance[i].Key / cyclingPerformance[i].Value)
                        });
                    }

                    if (i < cyclingPerformance.Count - 1 && cyclingPerformance[i + 1].Value == 0)
                    {
                        // If there's no information for a given distance, we just use the previous distance (but 5% slower, just an arbitrary value, no science here)
                        viewModel.CyclingPerformance.Add(new ActivityBase
                        {
                            Distance = cyclingPerformance[i + 1].Key,
                            Performance = viewModel.CyclingPerformance[i].Performance * 0.95,
                            Time = TimeSpan.FromHours(cyclingPerformance[i + 1].Key / viewModel.CyclingPerformance[i].Performance * 0.95)
                        });
                    }
                }

                viewModel.CyclingPerformance.RemoveAt(0);
            }

            if (swimmingPerformance.Sum(s => s.Value) == 0)
            {
                viewModel.SwimmingPerformance = null;
            }
            else
            {
                for (var i = 0; i < swimmingPerformance.Count; i++)
                {
                    if (swimmingPerformance[i].Value > 0)
                    {
                        viewModel.SwimmingPerformance.Add(new ActivityBase
                        {
                            Distance = swimmingPerformance[i].Key,
                            Performance = swimmingPerformance[i].Value,
                            Time = TimeSpan.FromMinutes(swimmingPerformance[i].Key / 100 * swimmingPerformance[i].Value)
                        });
                    }

                    if (i < swimmingPerformance.Count - 1 && swimmingPerformance[i + 1].Value == 0)
                    {
                        // If there's no information for a given distance, we just use the previous distance (but 5% slower, just an arbitrary value, no science here)
                        viewModel.SwimmingPerformance.Add(new ActivityBase
                        {
                            Distance = swimmingPerformance[i + 1].Key,
                            Performance = viewModel.SwimmingPerformance[i].Performance * 1.05,
                            Time = TimeSpan.FromMinutes(swimmingPerformance[i + 1].Key / 100 * viewModel.SwimmingPerformance[i].Performance * 1.05)
                        });
                    }
                }

                viewModel.SwimmingPerformance.RemoveAt(0);
            }

            return View("Index", viewModel);
        }

        public async Task<ActionResult> Callback()
        {
            var authenticator = StravaAuthenticator.CreateAuthenticator(RedirectUrl, HttpContext);
            await authenticator.OnPageLoaded(RedirectUrl, Request.QueryString.ToUriComponent());
            return RedirectToAction("Index");
        }
    }
}